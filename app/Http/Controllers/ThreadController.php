<?php

namespace App\Http\Controllers;

use Auth;
use App\MessageThread;

use Illuminate\Http\Request;
use App\Message as Message;

class ThreadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id = false, Request $request)
    {
        if ($id) {
            $response['edit'] = true;
            $thread = MessageThread::find($id);
            $response['thread'] = $thread;
            $response['messages'] = $thread->messages;
        } else {
            $response['edit'] = false;
        }

        return view('thread', $response);
    }

    public function save($id = false, Request $request)
    {
        if ($id) {
            $thread = MessageThread::find($id);
        } else {
            $thread = new MessageThread();
            $thread->user_id = Auth::user()->id;
            $thread->title = ($request->title) ? $request->title : 'no title added';
            $thread->save();
        }

        $message = new Message();
        $message->message_thread_id = $thread->id;
        $message->user_id = Auth::user()->id;
        $message->message = $request->message;
        $message->save();

        $response['edit'] = true;
        $response['thread'] = $thread;
        $response['messages'] = $thread->messages;

        return redirect('thread/' . $thread->id);
    }

    public function delete($id, Request $request)
    {

        $thread = Message::find($id)->delete();

        return redirect('thread/' . $request->thread_id);
    }
}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <div class="card-header row">
                    <div class="col-md-10">{{(Auth::user()->hasRole('admin'))?'All Threads Messages':'Your Message Threads'}}</div>
                    <div class="col-md-2 text-right"><a href="thread">Add New</a></div>
                </div>

                <div class="card-body">
                    
                        <table class="table">
                                <thead>
                                  <tr>
                                    <th scope="col">Title</th>
                                    <th scope="col">User</th>
                                    <th scope="col">View / Edit</th>
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach ($threads as $thread)
                                    <td scope="row">
                                            <a>{{ $thread->title }}</a>
                                    </td>
                                    <td>      
                                        <a>{{ $thread->user->name }}</a>
                                    </td>
                                    <td>
                                        <a href="/thread/{{ $thread->id }}">Edit / View</a>
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                            </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

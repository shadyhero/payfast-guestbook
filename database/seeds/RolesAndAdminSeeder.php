<?php

use Illuminate\Database\Seeder;

class RolesAndAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $roles = [
            'admin' => 'This is the admin user with full rights',
            'user'  => 'This is a regular user with limited rights'
        ];

        foreach ($roles as $rolename => $description) {

            $role = new App\Role();
            $role->name = $rolename;
            $role->description = $description;
            $role->save();
        }

        $admin_role = App\Role::where('name', 'admin')->first();

        $admin = new App\User();
        $admin->name = 'Admin Staff';
        $admin->email = 'admin@payfast-admin.co.za';
        $admin->password = bcrypt('secret');
        $admin->save();
        $admin->roles()->attach($admin_role);
    }
}

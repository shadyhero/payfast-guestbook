@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <div class="card-header">
                    
                    {{($edit)?'Edit '. $thread->title:'New Thread'}}
                </div>

                <div class="card-body">

                    @if (isset($messages))
                        <span>Message Thread:</span><br /><br />
                        @foreach ($messages as $message)
                        <div class="row">
                            <div class="col-md-8">
                                <small>{{$message->user->name}}</small>
                            </div>
                            <div class="col-md-4 text-right">
                                <small>{{date('d/m @ H:i', strtotime($message->updated_at))}}</small>
                            </div>
                        </div>
                        <p style="border:1px solid #000; background:#ccc; padding:20px; margin-bottom:0;">
                            {{ $message->message }}
                            @if (Auth::user()->hasRole('admin'))
                            <form method="post" action="/thread/{{$message->id}}">
                                @csrf
                                <input type="hidden" name="_method" value="delete" />
                                <input type="hidden" name="thread_id" value="{{$thread->id}}" />
                                <button type="submit" style="padding:2px; font-size:10px;">delete</button>
                            </form>
                            @endif
                        </p>
                        @endforeach
                    @endif
                    
                    <br /><br />

                    <form method="post" action="/thread/{{(isset($thread))?$thread->id:''}}">
                        @csrf
                        @if(!$edit)
                            <input name="title" placeholder="Enter Thread Title Here...">
                            <br /><br />
                        @endif
                        <span>Add new message:</span>
                        <textarea name='message' style="width: 100%" rows="7"></textarea>
                        <button type="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $fillable = [
        'message_thread_id', 'user_id', 'message'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function message_thread()
    {
        return $this->belongsTo('App\MessageThread');
    }
}

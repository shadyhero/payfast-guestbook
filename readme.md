<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## About Laravel

To install this project, run the following:

```
$ git clone git@bitbucket.org:shadyhero/payfast-guestbook.git
$ cd payfast-guestbook
$ composer update
$ cp .env.example .env
then add in your database details in the .env
$ php artisan key:generate
$ php artisan migrate:fresh --seed
```

## Created by

Shaheed Samsodien
Shaheed.samsodien@yahoo.com
<a href="https://www.linkedin.com/in/shaheedsamsodien/">LinkedIn</a>

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
